import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, SafeAreaView, Button} from 'react-native';

import Amplify, {Auth} from 'aws-amplify';
import config from './src/aws-exports';
import {withAuthenticator} from 'aws-amplify-react-native';
// import AsyncStorage from '@react-native-async-storage/async-storage';
import ReactNativeBiometrics from 'react-native-biometrics';

type CognitoUserName = {
  username: string;
};

Amplify.configure(config);

// AsyncStorage.getAllKeys((err, keys) => {
//   if (err) {
//     return;
//   }
//   AsyncStorage.multiGet(keys, (error, stores) => {
//     stores.map((result, i, store) => {
//       console.log({[store[i][0]]: store[i][1]});
//       return true;
//     });
//   });
// });

const App = () => {
  const [user, setUser] = useState<CognitoUserName | null>(null);
  const [supportedBiometric, setSupportedBiometric] = useState('');

  useEffect(() => {
    Auth.currentAuthenticatedUser({
      bypassCache: false, // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
    })
      .then(response => setUser(response))
      .catch(err => console.log(err));
  }, []);

  useEffect(() => {
    ReactNativeBiometrics.isSensorAvailable().then(resultObject => {
      const {available, biometryType} = resultObject;

      if (available && biometryType === ReactNativeBiometrics.TouchID) {
        setSupportedBiometric('TouchID is supported');
      } else if (available && biometryType === ReactNativeBiometrics.FaceID) {
        setSupportedBiometric('FaceID is supported');
      } else if (
        available &&
        biometryType === ReactNativeBiometrics.Biometrics
      ) {
        setSupportedBiometric('Biometrics is supported');
      } else {
        setSupportedBiometric('Biometrics not supported');
      }
    });
  }, []);

  const signInUsingBioMetrics = () => {
    // const epochTimeSeconds = Math.round(new Date().getTime() / 1000).toString();

    ReactNativeBiometrics.simplePrompt({
      promptMessage: 'Sign in',
    }).then(resultObject => {
      const {success} = resultObject;

      if (success) {
        console.log(resultObject);
      }
    });
  };

  const displayUsername = user ? user.username : '';

  return (
    <SafeAreaView style={styles.screen}>
      <Text style={styles.title}>Welcome {displayUsername}</Text>
      <Text style={styles.title}>{supportedBiometric}</Text>
      <Button onPress={() => Auth.signOut} title="Sign Out" />
      <Button
        onPress={signInUsingBioMetrics}
        title="Sign in using Biometrics"
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: '600',
  },
});

export default withAuthenticator(App);
